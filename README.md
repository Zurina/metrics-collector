# Metrics collector 

It runs as a Node application serving three different metrics. 
It runs with pm2, so if it goes down, pm2 pulls it back up again and runs the application. 
It pings both the frontend and the backend as well as measuring the response time for calls to the database. 
The metrics can be seen on Grafana on following endpoint: [server-endpoint](http://138.68.106.203:3002)

The credentials for login is admin/admin.

## Dependencies

- [PM2](https://pm2.keymetrics.io/)
- [Gauge](https://www.npmjs.com/package/gauge)
- [Node-fetch](https://www.npmjs.com/package/node-fetch)
- [Prom-client](https://www.npmjs.com/package/prom-client)
- [Mysql2](https://www.npmjs.com/package/mysql2)
- [Express](https://www.npmjs.com/package/express)

