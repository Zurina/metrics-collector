const client = require('prom-client');
const express = require('express');
const fetch = require('node-fetch');
const mysql = require('mysql2');

const server = express();

const registry = new client.Registry()
const metric_backend_health_check = new client.Gauge({ name: 'metric_backend_health_check', help: 'metric_help' });
const metric_frontend_health_check = new client.Gauge({ name: 'metric_frontend_health_check', help: 'metric_help' });
const metric_database_response_time = new client.Gauge({ name: 'metric_database_response_time', help: 'metric_help' });

registry.registerMetric(metric_backend_health_check);
registry.registerMetric(metric_frontend_health_check);
registry.registerMetric(metric_database_response_time);

//Backend check
setInterval(() => {
    fetch('http://46.101.241.48:4567')
        .then(res => {
            if (res.status == 200) metric_backend_health_check.set(1)
            else metric_backend_health_check.set(0)
        })
        .catch(err => {
            metric_backend_health_check.set(0)
        })
}, 5000);

//Frontend check
setInterval(() => {
    fetch('http://46.101.241.48:9090/frontend')
        .then(res => {
            if (res.status == 200) metric_frontend_health_check.set(1)
            else metric_frontend_health_check.set(0)
        })
        .catch(err => {
            metric_frontend_health_check.set(0)
        })
}, 5000);

//Database response time check
setInterval(() => {
    const connection = mysql.createConnection({
        host: '46.101.241.48',
        user: 'root',
        password: 'soft2019Backend',
        database: 'CarRental'
    });

    var start = new Date()

    connection.query(
        'SELECT 1',
        function (err, results, fields) {
            var end = new Date() - start
            metric_database_response_time.set(end)
        }
    );
    connection.end()
}, 5000);

server.get('/metrics', (req, res) => {
    res.set('Content-Type', registry.contentType);
    res.end(registry.metrics());
});

console.log('Server listening to 8282, metrics exposed on /metrics endpoint');
server.listen(8282);
